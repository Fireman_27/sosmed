<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/register', function () {
//     return view('layouts/register');
// });

// Route::get('/login', function () {
//     return view('layouts/login');
// });

// Route::get('/profile', function () {
//     return view('layouts/profile');
// });

// Route::get('/', function () {
//     return view('layouts/home');
// });

// Route::get('/profile/edit', function () {
//     return view('layouts/editprofile');
// });


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/', function () {
    return view('welcome');
})->middleware('web');

Route::group(['middleware' => ['web']], function () {
    //
});

Route::middleware(['web', 'subscribed'])->group(function () {
    //
});

Route::resource('Dash','DashController');

Route::resource('Profiles','ProfileController');

