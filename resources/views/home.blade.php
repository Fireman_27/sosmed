@extends('layouts.app')

{{-- @extends('master') --}}

@push('css')    

 <style type="text/css">
        #comments {
        margin-top:60px;
        }
        #comments .comment {
            margin:40px 0;
        }
        #comments a.replyBtn {
            float:right;
            font-size:11px;
            text-transform:uppercase;
        }
        #comments span.user-avatar {
            background:#eee;
            width:64px; height:64px;
            float:left;
            margin-right:10px;
        }

        ul.comment {
            margin-bottom:30px;
        }
        li.comment {
            position:relative;
            margin-bottom:25px;
            font-size:13px;
        }
        li.comment p {
            margin:0; padding:0;
        }
        li.comment img.avatar {
            position:absolute;
            left:0; top:0;
            display:inline-block;
        }
        li.comment.comment-reply img.avatar {
            left:6px; top:6px;
        }
        li.comment .comment-body {
            position:relative;
            padding-left:60px;
        }
        li.comment.comment-reply {
            margin-left:60px;
            background-color:#fafafa;
            padding:6px;
            margin-bottom:6px;
        }
        li.comment a.comment-author {
            margin-bottom:6px;
            display:block;
        }
        li.comment a.comment-author span {
            font-size:15px;
        }
        li.comment-relpy {
            margin-top:-15px;
            margin-bottom:20px;
        }
        time.datebox strong {
            padding: 2px 0;
            color: #fff;
            background-color:#1c2b36;
            display:block;
            text-align:center;
        }
        time.datebox span {
            font-size: 15px;
            color: #2f2f2f;
            display:block;
            text-align:center;
        }



        /**    17. Panel
        *************************************************** **/
        /* pannel */
        .panel {
            position:relative;

            background:transparent;

            -webkit-border-radius: 0;
            -moz-border-radius: 0;
                    border-radius: 0;

            -webkit-box-shadow: none;
            -moz-box-shadow: none;
                    box-shadow: none;
        }
        .panel.fullscreen .accordion .panel-body,
        .panel.fullscreen .panel-group .panel-body {
            position:relative !important;
            top:auto !important;
            left:auto !important;
            right:auto !important;
            bottom:auto !important;
        }
            
        .panel.fullscreen .panel-footer {
            position:absolute;
            bottom:0;
            left:0;
            right:0;
        }


        .panel>.panel-heading {
            text-transform: uppercase;

            -webkit-border-radius: 0;
            -moz-border-radius: 0;
                    border-radius: 0;
        }
        .panel>.panel-heading small {
            text-transform:none;
        }
        .panel>.panel-heading strong {
            font-family:Arial,Helvetica,Sans-Serif;
        }
        .panel>.panel-heading .buttons {
            display:inline-block;
            margin-top:-3px;
            margin-right:-8px;
        }
        .panel-default>.panel-heading {
            padding: 15px 15px;
            background:#fff;
        }
        .panel-default>.panel-heading small {
            color:#9E9E9E;
            font-size:12px;
            font-weight:300;
        }
        .panel-clean {
            border: 1px solid #ddd;
            border-bottom: 3px solid #ddd;

            -webkit-border-radius: 0;
            -moz-border-radius: 0;
                    border-radius: 0;
        }
        .panel-clean>.panel-heading {
            padding: 11px 15px;
            background:#fff !important;
            color:#000;	
            border-bottom: #eee 1px solid;
        }
        .panel>.panel-heading .btn {
            margin-bottom: 0 !important;
        }

        .panel>.panel-heading .progress {
            background-color:#ddd;
        }

        .panel>.panel-heading .pagination {
            margin:-5px;
        }

        .panel-default {
            border:0;
        }

        .panel-light {
            border:rgba(0,0,0,0.1) 1px solid;
        }
        .panel-light>.panel-heading {
            padding: 11px 15px;
            background:transaprent;
            border-bottom:rgba(0,0,0,0.1) 1px solid;
        }

        .panel-heading a.opt>.fa {
            display: inline-block;
            font-size: 14px;
            font-style: normal;
            font-weight: normal;
            margin-right: 2px;
            padding: 5px;
            position: relative;
            text-align: right;
            top: -1px;
        }

        .panel-heading>label>.form-control {
            display:inline-block;
            margin-top:-8px;
            margin-right:0;
            height:30px;
            padding:0 15px;
        }
        .panel-heading ul.options>li>a {
            color:#999;
        }
        .panel-heading ul.options>li>a:hover {
            color:#333;
        }
        .panel-title a {
            text-decoration:none;
            display:block;
            color:#333;
        }

        .panel-body {
            background-color:#fff;
            padding: 15px;

            -webkit-border-radius: 0;
            -moz-border-radius: 0;
                    border-radius: 0;
        }
        .panel-body.panel-row {
            padding:8px;
        }

        .panel-footer {
            font-size:12px;
            border-top:rgba(0,0,0,0.02) 1px solid;
            background-color:rgba(0255,255,255,1);

            -webkit-border-radius: 0;
            -moz-border-radius: 0;
                    border-radius: 0;
        }

 </style>
@endpush

@section('content')
<div class="container">
<div class="row justify-content-center">
<div id="overview" class="tab-pane active">
    <form class="well">
        <textarea rows="2" class="form-control" placeholder="What's on your mind?"></textarea>
        <div class="margin-top-10">
            <button type="submit" class="btn btn-sm btn-primary pull-right">Post</button>
            <a href="#" class="btn btn-link profile-btn-link" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Add a Location"><i class="fa fa-map-marker"></i></a>
            <a href="#" class="btn btn-link profile-btn-link" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Add Photo"><i class="fa fa-camera"></i></a>
            <a href="#" class="btn btn-link profile-btn-link" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Add File"><i class="fa fa-file"></i></a>
        </div>
    </form>

    <hr class="invisible half-margins">
    <!-- COMMENT -->
    <ul class="comment list-unstyled padding-10">
        <li class="comment">
            <!-- avatar -->
            <img class="avatar" src="https://bootdey.com/img/Content/avatar/avatar1.png" width="50" height="50" alt="avatar">
            <!-- comment body -->
            <div class="comment-body"> 
                <a href="#" class="comment-author">
                    <small class="text-muted pull-right"> 12 Minutes ago </small>
                    <span>Melisa Doe</span>
                </a>
                <p>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh 
                    euismod tincidunt laoreet dolore magna aliquam tincidunt erat volutpat laoreet 
                    dolore magna aliquam tincidunt erat volutpat.  
                </p>
            </div><!-- /comment body -->

            <!-- options -->
            <ul class="list-inline size-11 margin-top-10">
                <li>
                    <a href="#" class="text-info"><i class="fa fa-reply"></i> Reply</a>
                </li>
                <li>
                    <a href="#" class="text-success"><i class="fa fa-thumbs-up"></i> Like</a>
                </li>
                <li>
                    <a href="#" class="text-muted">Show All Comments (36)</a>
                </li>
                <li class="pull-right">
                    <a href="#" class="text-danger">Delete</a>
                </li>
                <li class="pull-right">
                    <a href="#" class="text-primary">Edit</a>
                </li>
            </ul>
        </li><!-- /options -->

        <li class="comment comment-reply">

            <!-- avatar -->
            <img class="avatar" src="https://bootdey.com/img/Content/avatar/avatar2.png" width="35" height="35" alt="avatar">

            <!-- comment body -->
            <div class="comment-body"> 
                <a href="#" class="comment-author">
                    <small class="text-muted pull-right"> 4 Minutes ago </small>
                    <span>Ioana Doe</span>
                </a>
                <p>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy! <i class="fa fa-smile-o green"></i> 
                </p>
            </div><!-- /comment body -->

            <!-- options -->
            <ul class="list-inline size-11">
                <li>
                    <a href="#" class="text-success"><i class="fa fa-thumbs-up"></i> Like</a>
                </li>
                <li class="pull-right">
                    <a href="#" class="text-danger">Delete</a>
                </li>
                <li class="pull-right">
                    <a href="#" class="text-primary">Edit</a>
                </li>
            </ul><!-- /options -->

        </li>

        <li class="comment comment-reply">

            <!-- avatar -->
            <img class="avatar" src="https://bootdey.com/img/Content/avatar/avatar3.png" width="35" height="35" alt="avatar">

            <!-- comment body -->
            <div class="comment-body"> 
                <a href="#" class="comment-author">
                    <small class="text-muted pull-right"> a moment ago </small>
                    <span>Simona Doe</span>
                </a>
                <p>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy! <i class="fa fa-smile-o green"></i> 
                </p>
            </div><!-- /comment body -->

            <!-- options -->
            <ul class="list-inline size-11">
                <li>
                    <a href="#" class="text-success"><i class="fa fa-thumbs-up"></i> Like</a>
                </li>
                <li class="pull-right">
                    <a href="#" class="text-danger">Delete</a>
                </li>
                <li class="pull-right">
                    <a href="#" class="text-primary">Edit</a>
                </li>
            </ul><!-- /options -->

        </li>
    </ul>
    <!-- /COMMENT -->

    <!-- COMMENT -->
    <ul class="comment list-unstyled padding-10">
        <li class="comment">

            <!-- avatar -->
            <img class="avatar" src="https://bootdey.com/img/Content/avatar/avatar4.png" width="50" height="50" alt="avatar">

            <!-- comment body -->
            <div class="comment-body"> 
                <a href="#" class="comment-author">
                    <small class="text-muted pull-right"> 12 Minutes ago </small>
                    <span>Melisa Doe</span>
                </a>
                <p>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh 
                    euismod tincidunt laoreet dolore magna aliquam tincidunt erat volutpat laoreet 
                    dolore magna aliquam tincidunt erat volutpat.  
                </p>
            </div><!-- /comment body -->

            <!-- options -->
            <ul class="list-inline size-11 margin-top-10">
                <li>
                    <a href="#" class="text-info"><i class="fa fa-reply"></i> Reply</a>
                </li>
                <li>
                    <a href="#" class="text-success"><i class="fa fa-thumbs-up"></i> Like</a>
                </li>
                <li class="pull-right">
                    <a href="#" class="text-danger">Delete</a>
                </li>
                <li class="pull-right">
                    <a href="#" class="text-primary">Edit</a>
                </li>
            </ul>
        </li><!-- /options -->

        <li class="comment comment-reply">

            <!-- avatar -->
            <img class="avatar" src="https://bootdey.com/img/Content/avatar/avatar5.png" width="35" height="35" alt="avatar">

            <!-- comment body -->
            <div class="comment-body"> 
                <a href="#" class="comment-author">
                    <small class="text-muted pull-right"> 4 Minutes ago </small>
                    <span>Ioana Doe</span>
                </a>
                <p>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy! <i class="fa fa-smile-o green"></i> 
                </p>
            </div><!-- /comment body -->

            <!-- options -->
            <ul class="list-inline size-11">
                <li>
                    <a href="#" class="text-success"><i class="fa fa-thumbs-up"></i> Like</a>
                </li>
                <li class="pull-right">
                    <a href="#" class="text-danger">Delete</a>
                </li>
                <li class="pull-right">
                    <a href="#" class="text-primary">Edit</a>
                </li>
            </ul><!-- /options -->

        </li>

        <li class="comment comment-reply">

            <!-- avatar -->
            <img class="avatar" src="https://bootdey.com/img/Content/avatar/avatar6.png" width="35" height="35" alt="avatar">

            <!-- comment body -->
            <div class="comment-body"> 
                <a href="#" class="comment-author">
                    <small class="text-muted pull-right"> a moment ago </small>
                    <span>Simona Doe</span>
                </a>
                <p>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy! <i class="fa fa-smile-o green"></i> 
                </p>
            </div><!-- /comment body -->

            <!-- options -->
            <ul class="list-inline size-11">
                <li>
                    <a href="#" class="text-success"><i class="fa fa-thumbs-up"></i> Like</a>
                </li>
                <li class="pull-right">
                    <a href="#" class="text-danger">Delete</a>
                </li>
                <li class="pull-right">
                    <a href="#" class="text-primary">Edit</a>
                </li>
            </ul><!-- /options -->

        </li>
        <li>
            <div class="input-group">
                <input id="btn-input" type="text" class="form-control" placeholder="Type your message...">
                <span class="input-group-btn">
                    <button class="btn btn-primary" id="btn-chat">
                        <i class="fa fa-reply"></i> Reply
                    </button> 
                </span>
            </div>
        </li>
    </ul>
    <!-- /COMMENT -->
</div>
</div>
</div> 
@endsection

@push('scripts')
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
@endpush

{{-- @section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
